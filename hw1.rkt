#lang racket
(provide (all-defined-out)) ;; so we can put tests in a second file

(define (ins_beg el lst)
  (cons el lst))

(define (ins_end el lst)
  (append lst (list el)))

(define (count_elements lst)
  (define (count lst acc)
        (cond
          [(null? lst) ; base case
           acc]
          [else
           (count (cdr lst) (+ acc 1))] ; recursive case
          ))
    (count lst 0))

(define (count_element el lst)
  (define (count lst acc)
    (cond
      [(null? lst) ; base case
       acc]
      [(eq? el (car lst))
       (count (cdr lst) (+ acc 1))]  ; recursive case
      [else
       (count (cdr lst) acc)]))  ; recursive case
  (count lst 0))

(define (count_element_ntr el lst)
    (cond
      [(null? lst) ; base case
       0]
      [(eq? el (car lst))
       (+ 1 (count_element_ntr el (cdr lst)))]  ; recursive case
      [else
       (count_element_ntr el (cdr lst))]))  ; recursive case

(define (atom? x) (not (or (pair? x) (null? x) (vector? x))))
(define (count_element_r el lst)
  (define (count lst acc)
    (cond
      [(null? lst) ; base case
       acc]
      [(atom? (car lst))
       (if (eq? el (car lst))
           (count (cdr lst) (+ acc 1))  ; recursive case
           (count (cdr lst) acc))]  ; recursive case
      [else
       (count (cdr lst) (+ (count (car lst) 0) acc))]))  ; recursive case
  (count lst 0))