#lang racket
(provide (all-defined-out)) ;; so we can put tests in a second file

(define (atom? x) (not (or (pair? x) (null? x) (vector? x))))

(define (smaller-child node) (cadr node))
(define (middle-value node) (car node))
(define (bigger-child node) (caddr node))
(define (single-element-node? node) (atom? node))
(define (empty-node? node) (null? node))

(define (binary-tree-to-sorted-list tree)
  (define (node-to-sorted-list node)
     (cond
       [(empty-node? node)
        '()] ;  base case
       [(single-element-node? node)
        (list node)] ;  base case
       [else
        (append (node-to-sorted-list (smaller-child node)) (append (list (middle-value node)) (node-to-sorted-list (bigger-child node))))]))
  (node-to-sorted-list tree))

(define (is-inside-tree? tree el)
  (define (is-inside-node? node)
    (cond
      [(empty-node? node)
       #f] ;  base case
      [(single-element-node? node)
       (eq? el node)] ;  base case
      [(> el (middle-value node))
       (is-inside-node? (bigger-child node))]
      [(< el (middle-value node))
       (is-inside-node? (smaller-child node))]
      [else
       (eq? (middle-value node) el)]))
  (is-inside-node? tree))

(define (insert-into-tree tree el)
  (define (insert-into-node node)
    (cond
      [(empty-node? node)
       el] ;  base case
      [(single-element-node? node)
       (if (>= el node) (list node '() el) (list node el '()))]
      [(>= el (middle-value node))
       (list (middle-value node) (smaller-child node) (insert-into-node (bigger-child node)))]
      [else
       (list (middle-value node) (insert-into-node (smaller-child node)) (bigger-child node))]))
  (insert-into-node tree))

(define (insert-list-into-tree tree lst)
  (if (null? lst)
      tree ;  base case
      (insert-list-into-tree (insert-into-tree tree (car lst)) (cdr lst))))

(define (list-to-tree lst)
  (insert-list-into-tree (list (car lst) '() '()) (cdr lst)))

;;;;; predicator


(define (pred-insert-into-tree pred tree el)
  (define (insert-into-node node)
    (cond
      [(empty-node? node)
       el] ;  base case
      [(single-element-node? node)
       (if (pred el node) (list node '() el) (list node el '()))] ;  base case
      [(pred el (middle-value node))
       (list (middle-value node) (smaller-child node) (insert-into-node (bigger-child node)))]
      [else
       (list (middle-value node) (insert-into-node (smaller-child node)) (bigger-child node))]))
  (insert-into-node tree))

(define (pred-insert-list-into-tree pred tree lst)
  (if (null? lst)
      tree
      (pred-insert-list-into-tree pred (insert-into-tree tree (car lst)) (cdr lst))))

(define (pred-list-to-tree pred lst)
  (pred-insert-list-into-tree pred (list (car lst) '() '()) (cdr lst)))

(define (sort-asc lst)
  (pred-list-to-tree (λ(a b) (> a b)) lst))

(define (sort-desc lst)
  (pred-list-to-tree (λ(a b) (< a b)) lst))

(define (last-digit digit)
  (if (< digit 10) digit (last-digit (/ digit 10))))

(define (sort-last-digit-asc lst)
  (pred-list-to-tree
   (λ(a b) (> (last-digit a) (last-digit b))) lst))

